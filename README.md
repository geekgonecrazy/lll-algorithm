# LLL-Algorithm

#### 介绍
The Lenstra–Lenstra–Lovász (LLL) lattice basis reduction algorithm is a polynomial time lattice reduction algorithm invented by Arjen Lenstra, Hendrik Lenstra and László Lovász in 1982.

#### 使用说明

- 通过命令行使用本程序.
- 例如：`.\lll.exe --data=data.txt --M=3 --N=3 --verbose`.
- --data参数 指定数据的来源，也就是从哪个文件里读取数据。读取数据时默认一行代表一个列向量，也就是一个`lattice basis`.
- --M参数 指定有多少个向量
- --N参数 指定向量有多少维
- --verbose参数 （可选） 如果加上，则会输出计算的过程

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

