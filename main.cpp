#include "option_parser.h"
#include "LLL.h"
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <memory>
#include <fstream>
#include <iostream>
static void help(int exit_code = 1)
{
	fprintf(stderr, "Lenstra-Lenstra-Lovasz lattice basis reduction algorithm"  "\n\n");
	fprintf(stderr, "usage: spike --data=data.txt --m=3 --n=3\n");
	exit(exit_code);
}

static void suggest_help()
{
	fprintf(stderr, "Try './lll --help' for more information.\n");
	exit(1);
}

int main(int argc, char** argv)
{
	const char* data_path = nullptr;
	int M = 0;
	int N = 0;
	bool verbose = false;

	option_parser_t parser;
	parser.help(&suggest_help);
	parser.option('h', "help", 0, [&](const char* s) {help(0); });
	
	parser.option(0, "data", 1, [&](const char* s) {data_path = s; });
	parser.option(0, "m", 1, [&](const char* s) { M = atoi(s); });
	parser.option(0, "n", 1, [&](const char* s) { N = atoi(s); });
	parser.option(0, "verbose", 0, [&](const char* s) { verbose = true; });

	auto argv1 = parser.parse(argv);
	if (!data_path) data_path = "data.txt";
	if (M == 0 || N == 0 ) { M = N = 3; }
	std::cout << "M: " << M << std::endl;
	std::cout << "N: " << N << std::endl;
	std::cout << "data_path: " << data_path << std::endl;
	double* vec = (double*)malloc(M * N * sizeof(double));
	double* ortho = (double*)malloc(M * N * sizeof(double));

	getVectorFromFile(data_path, vec, N);

	if (verbose)
		lll_algorithm(vec, ortho, M, N);
	else
		lll_algorithm_(vec, ortho, M, N);

	free(vec);
	free(ortho);

}