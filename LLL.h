#ifndef __LLL_H__
#define __LLL_H__

#include <stdio.h>
#include <stdlib.h>
#include <cblas.h>
#include <string.h>
#include <math.h>
#include <ctype.h>


void getVectorFromFile(const char* file_name, double* Vectors, int N);
void ortho(double* input_vectors, double* output_vectors, int M, int N);
void fetch_vector(double* vectors, int idx, double* vector_container, int);
void write_vector(double* vectors, int idx, double* vector_container, int);
void ortho_lll(double* input_vectors, double* output_vectors, int M, int N);
void swap_vector(double* vectors, int idx1, int idx2, int);
void lll_algorithm(double* lattice_basis, double* ortho_vectors, int, int);
int check_lovasz(double* lattice, double* ortho_vectors_, int, int);
void print_vectors(double* vectors_, int, int);


void getVectorFromFile(const char* file_name, double* Vectors, int N) {
    //*从文件中读取数据
    char line[1024];
    const char s[2] = " ";
    FILE* stream = fopen(file_name, "r");
    if (!stream) {
        fprintf(stdout, "fopen():%s", strerror(errno));
        exit(EXIT_FAILURE);
    }
    size_t row = 0;
    while (fgets(line, 1024, stream)) {
        char* token = strtok(line, s);
        size_t col = 0;
        while (token != NULL) {
            *(Vectors + (N * row + col)) = atof(token);
            token = strtok(NULL, s);
            col++;
        }

        row++;
    }
    free(stream);
}

void ortho(double* input_vectors, double* output_vectors, int M, int N) {
    //只进行施密特正交化
    double* current_vector = (double*)malloc(N * sizeof(double));
    double* former_vector = (double*)malloc(N * sizeof(double));
    double* former_vector_orthoed = (double*)malloc(N * sizeof(double));

    for (int j = 0; j < N; ++j) {// 第一个向量
        *(output_vectors + j) = *(input_vectors + j);
    }

    for (int i = 1; i < M; ++i) {
        fetch_vector(input_vectors, i, current_vector, N);//取第i个向量
        for (int j = i - 1; j >= 0; j--) {
            fetch_vector(input_vectors, j, former_vector, N);
            fetch_vector(output_vectors, j, former_vector_orthoed, N);
            double alpha = cblas_ddot(N, current_vector, 1, former_vector_orthoed, 1) / cblas_ddot(N, former_vector_orthoed, 1, former_vector_orthoed, 1);
            cblas_daxpy(N, -1 * alpha, former_vector, 1, current_vector, 1);
        }
        write_vector(output_vectors, i, current_vector, N);

    }
    free(current_vector);
    free(former_vector);
    free(former_vector_orthoed);
}
void ortho_lll(double* input_vectors, double* output_vectors, int M, int N) {
    /* input_vectors: 一组lattice基
     * output_vectors: 施密特正交化之后的lattice基
     * 同时对 input_vectors 中的向量进行 lll算法的 Reduction Step
     * */

    double* current_vector = (double*)malloc(N * sizeof(double));
    double* former_vector = (double*)malloc(N * sizeof(double));
    double* former_vector_orthoed = (double*)malloc(N * sizeof(double));
    double* current_vector_ortho = (double*)malloc(N * sizeof(double));

    for (int j = 0; j < N; ++j) {// 处理第一个向量
        *(output_vectors + j) = *(input_vectors + j);
    }

    for (int i = 1; i < M; ++i) {
        fetch_vector(input_vectors, i, current_vector, N);
        fetch_vector(input_vectors, i, current_vector_ortho, N);
        for (int j = i - 1; j >= 0; j--) {
            fetch_vector(input_vectors, j, former_vector, N);
            fetch_vector(output_vectors, j, former_vector_orthoed, N);
            double alpha = cblas_ddot(N, current_vector, 1, former_vector_orthoed, 1) / cblas_ddot(N, former_vector_orthoed, 1, former_vector_orthoed, 1);
            cblas_daxpy(N, -1 * alpha, former_vector, 1, current_vector_ortho, 1);
            double alpha_int = 0;
            if (alpha == 0.5 || alpha == -0.5)
                alpha_int = 0;
            else
                alpha_int = round(alpha);
            if (alpha_int != 0)
                cblas_daxpy(N, -1 * alpha_int, former_vector, 1, current_vector, 1);
        }
        write_vector(output_vectors, i, current_vector_ortho, N);
        write_vector(input_vectors, i, current_vector, N);
    }
    free(current_vector);
    free(former_vector);
    free(former_vector_orthoed);
    free(current_vector_ortho);
}

void fetch_vector(double* vectors, int idx, double* vector_container, int N) {
    //从输入的向量组中取出指定的向量供计算使用
    for (int i = 0; i < N; i++) {
        *(vector_container + i) = *(vectors + (idx * N + i));
    }
}
void write_vector(double* vectors, int idx, double* vector_container, int N) {
    //将计算结果写入到向量组中
    for (int i = 0; i < N; i++) {
        *(vectors + (idx * N + i)) = *(vector_container + i);
    }
}

void swap_vector(double* vectors, int idx1, int idx2, int N) {
    //交换两个向量
    double temp_;
    for (int i = 0; i < N; ++i) {
        temp_ = *(vectors + idx1 * N + i);
        *(vectors + idx1 * N + i) = *(vectors + idx2 * N + i);
        *(vectors + idx2 * N + i) = temp_;
    }
}

//算法主体部分
void lll_algorithm_(double* lattice_basis, double* ortho_vectors, int M, int N) {
    if (!lattice_basis || !ortho_vectors) {
        printf("Null vector pointers!\n");
        printf("Exit...\n");
        exit(EXIT_FAILURE);
    }
    ortho_lll(lattice_basis, ortho_vectors, M, N);
    //对lattice_basis执行reduce step，同时获得施密特正交基ortho_vectors


    while (check_lovasz(lattice_basis, ortho_vectors, M, N)) {
        //检查是否符合lavasz条件，若符合则停止，若不符合则交换向量基并重新运行reduce step
        ortho_lll(lattice_basis, ortho_vectors, M, N);
    }
    print_vectors(lattice_basis, M, N);
}

void lll_algorithm(double* lattice_basis, double* ortho_vectors, int M, int N) {
    if (!lattice_basis || !ortho_vectors) {
        printf("Null vector pointers!\n");
        printf("Exit...\n");
        exit(EXIT_FAILURE);
    }
    print_vectors(lattice_basis, M, N);
    printf("- - - - - - - - \n");
    ortho_lll(lattice_basis, ortho_vectors, M, N);
    print_vectors(lattice_basis, M, N);
    printf("- - - - - - - - - \n");
    while (check_lovasz(lattice_basis, ortho_vectors, M, N)) {
        print_vectors(lattice_basis, M, N);
        printf("- - - - - - - - - \n");

        ortho_lll(lattice_basis, ortho_vectors, M, N);
        print_vectors(lattice_basis, M, N);
        printf("- - - - - - - - - \n");

    }

}

int check_lovasz(double* lattice, double* ortho_vectors_, int M, int N) {
    //检查是否符合lovasz条件
    for (int i = 1; i < M; ++i) {
        double* b_i = (double*)malloc(N * sizeof(double));
        double* b_i_1 = (double*)malloc(N * sizeof(double));
        double* b_i_star = (double*)malloc(N * sizeof(double));
        fetch_vector(lattice, i, b_i, N);
        fetch_vector(ortho_vectors_, i - 1, b_i_1, N);
        fetch_vector(ortho_vectors_, i, b_i_star, N);
        double mu = cblas_ddot(N, b_i, 1, b_i_1, 1) / cblas_ddot(N, b_i_1, 1, b_i_1, 1);
        double norm2 = cblas_ddot(N, b_i_1, 1, b_i_1, 1);
        cblas_daxpy(N, mu, b_i_1, 1, b_i_star, 1);
        double norm1 = cblas_ddot(N, b_i_star, 1, b_i_star, 1);
        if (norm1 < 0.75 * norm2) {
            printf("swapping vectors\n");
            swap_vector(lattice, i, i - 1, N);
            return 1;
        }


    }
    return 0;
}

void print_vectors(double* vectors_, int M, int N) {
    for (int i = 0; i < M * N; ++i) {
        printf("%lf ", *(vectors_ + i));
        if ((i + 1) % N == 0) printf("\n");
    }
}

#endif